#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>
#include<assert.h>

struct Stack
{
int top;
unsigned capacity;
int* array;
};

void printStack(struct Stack *stack,int topCnt); 
int CreateStack(struct Stack **stack,int capacity);
int isFull(struct Stack *stack);
int isEmpty(struct Stack *stack);
int Push(struct Stack **stack,int data);
int Pop(struct Stack **stack);
int Top(struct Stack *stack);  
